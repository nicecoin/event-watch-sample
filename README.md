# event-watch-sample

Web3を使用したコントラクトのイベント監視サンプル

参考
https://qiita.com/satons/items/d9c2dabfbf513e44f78b

## 構築手順

プロジェクト初期化
```
$ cd git/event-watch-sample
$ npm init
// 入力要求は全てデフォルト（エンターキー）で応答
・・・
About to write to /Users/tools/git/event-watch-sample/package.json:

{
  "name": "event-watch-sample",
  "version": "1.0.0",
  "description": "Web3を使用したコントラクトのイベント監視サンプル",
  "main": "event-watch.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": "git+https://sjtaro001@bitbucket.org/nicecoin/event-watch-sample.git"
  },
  "author": "",
  "license": "ISC",
  "homepage": "https://bitbucket.org/nicecoin/event-watch-sample#readme"
}

Is this OK? (yes) <yes応答
```

web3 0.20.x インストール
```
$ npm info web3 versions
  // インストール可能なバージョン一覧が表示される
[ '0.20.4',
  '0.20.5',
  '0.20.6',
  '0.20.7',
  '1.0.0-beta.32',
  '1.0.0-beta.33',
  '1.0.0-beta.34',
  '1.0.0-beta.35',
  '1.0.0-beta.36',
  '1.0.0-beta.37',
  '1.0.0-beta.38',
  '1.0.0-beta.39',
  '1.0.0-beta.40',
  '1.0.0-beta.41',
  '1.0.0-beta1',
  '1.0.0-beta2' ]

$ npm install web3@0.20.7

```

event-watch.js 実装

```
var Web3 = require('web3');
const fs = require('fs');
var web3 = new Web3();

web3.setProvider(new web3.providers.HttpProvider("http://localhost:7545"));

//niceCoinのABI
const contract = JSON.parse(fs.readFileSync('../nice_coin_sample/build/contracts/NiceCoin.json', 'utf8'));
var ABI = contract.abi;
//console.log("ABI:",JSON.stringify(ABI)); 

//デプロイしたアドレス 
var address = '0x3c165Ba1b079AD62006f3Fddafd64c7A0F5989C6'

var niceCoin = web3.eth.contract(ABI).at(address);
var event = niceCoin.CTransfer();
//イベント監視
event.watch(function (error, result) {
 console.log('watching "CTransfer" event!');
  if (!error)
    console.log(result);
});

```

event-watch実行
```
$ node event-watch.js
```

実行中にniceCoinでtransferを行い、CTransferイベントが発生すると、イベントの内容がコンソール出力される
```
watching "CTransfer" event!
{ logIndex: 1,
  transactionIndex: 0,
  transactionHash:
   '0x06dfdd53bce628b0ed90cd41d9d49bf19df6aa986724bbbcbba89da8a648efa5',
  blockHash:
   '0x0a98434fb3b80ca883897583e7a8abe085a3aa2095241e351f08d2a1dd64849a',
  blockNumber: 14,
  address: '0x3c165ba1b079ad62006f3fddafd64c7a0f5989c6',
  type: 'mined',
  event: 'CTransfer',
  args:
   { _date: BigNumber { s: 1, e: 9, c: [Array] },
     _from: '0xd127c4a5d80d0d33d6be37d0b326d2816365cf8d',
     _to: '0xda965f2e7a61e48a68d67531c1074c5f92cfa833',
     _value: BigNumber { s: 1, e: 3, c: [Array] } } }
```

