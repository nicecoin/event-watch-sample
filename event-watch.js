var Web3 = require('web3');
const fs = require('fs');
var web3 = new Web3();

web3.setProvider(new web3.providers.HttpProvider("http://localhost:7545"));

//niceCoinのABI
const contract = JSON.parse(fs.readFileSync('../nice_coin_sample/build/contracts/NiceCoin.json', 'utf8'));
var ABI = contract.abi;
//console.log("ABI:",JSON.stringify(ABI)); 

//デプロイしたアドレス 
var address = '0x3c165Ba1b079AD62006f3Fddafd64c7A0F5989C6'

var niceCoin = web3.eth.contract(ABI).at(address);
var event = niceCoin.CTransfer();
//イベント監視
event.watch(function (error, result) {
 console.log('watching "CTransfer" event!');
  if (!error)
    console.log(result);
});
